<?php
namespace Infosophie\Api;

use Infosophie\Error;

class Api {

    private static $init = false;
    private static $pathInfo = '';
    private static $pathElements = [];
    private static $ressource = [];
    private static $class;
    private static $id;
    private static $subclass;
    private static $subid;
    private static $verb;
    private static $body;

    public static function ressource() {
        return self::$ressource;
    }

    public static function class() {
        if (self::init()) {
            return self::$class;
        }
    }

    public static function id() {
        if (self::init()) {
            return self::$id;
        }
    }

    public static function subclass() {
        if (self::init()) {
            return self::$subclass;
        }
    }

    public static function subid() {
        if (self::init()) {
            return self::$subid;
        }
    }

    public static function verb() {
        if (self::init()) {
            return self::$verb;
        }
    }

    public static function body() {
        if (self::init()) {
            return self::$body;
        }
    }

    public static function queryString() {
        if (self::init()) {
            return self::$queryString;
        }
    }


    private function init() {
        if (!self::$init) {
            self::$init = self::resolveQuery();
        }
        return self::$init;
    }

    private function resolveQuery() {
        self::$verb = $_SERVER['REQUEST_METHOD'];
        self::$body = json_decode(@file_get_contents('php://input'), true);
        self::$pathInfo = $_SERVER['PATH_INFO'];
        self::$pathElements = preg_split("/\//", preg_replace("/^\/+/", "", self::$pathInfo));
        self::$ressource = self::$pathElements[0];
        self::$class = self::$pathElements[1];
        self::$id = self::$pathElements[2];
        self::$subclass = self::$pathElements[3];
        self::$subid = self::$pathElements[4];
        if (empty(self::$body)) {
            self::$body = $_GET;
        }
        return true;
    }

}
